// LIBRARIES
import axios from 'axios'
import { Module } from 'vuex'

// ENDPOINTS
import { POST_LOGIN_ENDPOINT } from '@/utils/endpoints'

// UTILS
import { saveUser, clearUser } from '@/utils/auth'

// INTERFACES
import LoginPayload from '@/interfaces/auth/LoginPayload'
import UserState from '@/interfaces/auth/UserState'

// VUEX.STORE OPTIONS
export const auth: Module<UserState, UserState> = {
  state: {
    currentUser: null
  },
  mutations: {
    SET_USER (state: UserState, userResponse: string) {
      state.currentUser = userResponse
    },
    CLEAR_USER (state: UserState) {
      state.currentUser = null
      clearUser()
    }
  },
  actions: {
    async login ({ commit }, payload: LoginPayload) {
      try {
        const userResponse = await axios.post(`${POST_LOGIN_ENDPOINT}`, payload)
        saveUser(userResponse.data.token)
        commit('SET_USER', userResponse.data)
      } catch (error) {
        throw new Error(error)
      }
    }
  }
}
