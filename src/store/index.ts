// LIBRARIES
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

// STORE MODULES
import { auth } from '@/store/auth'
import { reports } from '@/store/reports'

// VUEX.STORE OPTIONS
Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    currentPage: 1
  },
  plugins: [vuexLocal.plugin],
  mutations: {
    SET_PAGE (state, page) {
      state.currentPage = page
    }
  },
  actions: {},
  modules: {
    auth, reports
  },
  getters: {
    getCurrentPage: state => state.currentPage
  }
})
