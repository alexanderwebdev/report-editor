// LIBRARIES
import axios from 'axios'

// ENDPOINTS
import { GET_REPORTS_ENDPOINT, POST_REPORTS_ENDPOINT } from '@/utils/endpoints'

// VUEX.STORE OPTIONS
export const reports = {
  state: {
    reports: []
  },
  mutations: {
    SET_REPORTS (state, reportResponse) {
      /* Here I merge response from API with state,
         to not to erase previous result from set specific report */
      state.reports = [...reportResponse, ...state.reports]
    },
    SET_REPORT (state, reportResponse) {
      state.reports.push(reportResponse)
    }
  },
  actions: {
    async getReports ({ commit }) {
      try {
        const reportsResponse = await axios.get(`${GET_REPORTS_ENDPOINT}`)
        commit('SET_REPORTS', reportsResponse.data)
      } catch (error) {
        throw new Error(error)
      }
    },
    async pushReport ({ commit }, payload) {
      try {
        const reportResponse = await axios.post(`${POST_REPORTS_ENDPOINT}/${payload.report.type}`)

        /* API returns a stub, so I pass the payload into mutation instead of response,
           to show the result of Store in Dashboard page */
        commit('SET_REPORT', payload)
      } catch (error) {
        throw new Error(error)
      }
    }
  }
}
