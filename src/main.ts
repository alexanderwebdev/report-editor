// LIBRARIES
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'

import '@babel/polyfill'
import 'mutationobserver-shim'
import './registerServiceWorker'

import './plugins/axios'
import './plugins/bootstrap-vue'

// VUE CONFIG
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
