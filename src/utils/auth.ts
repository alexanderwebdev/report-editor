export const isUser = function () {
  return localStorage.getItem('currentUserToken')
}

export const saveUser = function (user: string) {
  localStorage.setItem('currentUserToken', JSON.stringify(user))
}

export const clearUser = function () {
  localStorage.setItem('currentUserToken', '')
}
