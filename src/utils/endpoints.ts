export const POST_LOGIN_ENDPOINT = 'https://39c81456-e28f-4ca1-9487-d60ca5d9ef47.mock.pstmn.io/api/login'
export const GET_REPORTS_ENDPOINT = 'https://39c81456-e28f-4ca1-9487-d60ca5d9ef47.mock.pstmn.io/api/v2/reports'
export const POST_REPORTS_ENDPOINT = 'https://39c81456-e28f-4ca1-9487-d60ca5d9ef47.mock.pstmn.io/api/reports/'
