// LIBRARIES
import Vue from 'vue'
import VueRouter, { NavigationGuardNext, Route, RouteConfig } from 'vue-router'

// COMPONENTS
import LoginPage from '@/views/auth/LoginPage.vue'
import DashboardPage from '@/views/DashboardPage.vue'
import CreateReportPage from '@/views/CreateReportPage.vue'
import ReportEditor from '@/views/ReportEditor.vue'

// UTILS
import { isUser } from '@/utils/auth'

// ROUTER OPTIONS
Vue.use(VueRouter)

const authorized = (to: Route, from: Route, next: NavigationGuardNext) => {
  next(isUser() ? undefined : '/login')
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'DashboardPage',
    component: DashboardPage,
    meta: { layout: 'Authorized' },
    beforeEnter: authorized
  },
  {
    path: '/create-report',
    name: 'CreateReportPage',
    component: CreateReportPage,
    meta: { layout: 'Authorized' },
    beforeEnter: authorized
  },
  {
    path: '/report-editor/:type',
    name: 'ReportEditor',
    component: ReportEditor,
    meta: { layout: 'Authorized' },
    beforeEnter: authorized
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: LoginPage,
    meta: { layout: 'Authentication' },
    beforeEnter: (to, from, next) => {
      next(!isUser() ? undefined : '/')
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
