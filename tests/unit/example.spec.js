import Vue from 'vue'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import LoginPage from '@/views/auth/LoginPage.vue'
import { mount, createLocalVue } from '@vue/test-utils'
import router from '@/router'

const localVue = createLocalVue()
localVue.use(Vuex)

Vue.use(BootstrapVue)

describe('LoginPage', () => {
  it('calls the login action', async () => {
    const loginMock = jest.fn(() => localStorage.setItem('currentUserToken', 'test'))
    const credentials = {
      username: 'joe.doe@acme.com',
      password: 'password'
    }

    const store = new Vuex.Store({
      actions: {
        login: loginMock
      }
    })

    const wrapper = mount(LoginPage, {
      localVue,
      store,
      router,
      global: {
        mocks: {
          $route: {
            name: 'LoginPage',
            path: '/login'
          },
          $router: {
            push: jest.fn()
          }
        }
      }
    })

    const usernameInput = wrapper.find('input[type="email"]')
    await usernameInput.setValue(credentials.username)
    expect(wrapper.find('input[type="email"]').element.value).toBe(credentials.username)

    const passwordInput = wrapper.find('input[type="text"]')
    await passwordInput.setValue(credentials.password)
    expect(wrapper.find('input[type="text"]').element.value).toBe(credentials.password)

    wrapper.find('.login-action-button').trigger('submit')
    expect(loginMock).toHaveBeenCalled()
  })
})
