/* global context, it, cy, Cypress */
context('Login test', () => {
  before(function () {
    cy.clearLocalStorage()
  })
  describe('Login', () => {
    it('Should login', () => {
      cy.visit(Cypress.env('loginUrl'))

      cy.get('[data-cy=email-input]')
        .clear()
        .type(Cypress.env('validEmail'))

      cy.get('[data-cy=password-input]')
        .clear()
        .type(Cypress.env('validPassword'))

      cy.get('[data-cy=submit-button]').click()
    })
  })

  describe('Create report', () => {
    it('Should create sales report', () => {
      cy.server().route('GET', '/api/v2/reports').as('reports')
      cy.wait('@reports').its('status').should('be', 200)

      cy.get('[data-cy=create-report-button]')
        .click()

      cy.wait(1000)

      cy.get('[data-cy=create-sales-report-button]')
        .click()

      cy.wait(1000)

      cy.get('[data-cy=report-name-input]')
        .clear()
        .type('New rental report')

      cy.get('[data-cy=report-status-input]')
        .find('.custom-select')
        .select('expired')

      cy.get('[data-cy=report-info-name-input]')
        .clear()
        .type('New info')

      cy.get('[data-cy=report-info-area-input]')
        .clear()
        .type('Specific area')

      cy.get('[data-cy=report-customize-input]')
        .clear()
        .type('Custom info')

      cy.get('[data-cy=create-sales-save-button]')
        .click()
    })
  })
})
